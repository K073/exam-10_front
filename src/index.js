import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from "react-router-dom";
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import reducer from "./store/reducer";
import { Provider } from 'react-redux';

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

const app =
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>;

ReactDOM.render(app , document.getElementById('root'));
registerServiceWorker();

