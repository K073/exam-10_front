import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://exam-9-best.firebaseio.com/'
});

export default instance;