import axios from '../axios'

export const FETCH_CONTACT_REQUEST = 'FETCH_CONTACT_REQUEST';
export const FETCH_CONTACT_SUCCESS = 'FETCH_CONTACT_SUCCESS';
export const FETCH_CONTACT_ERROR = 'FETCH_CONTACT_ERROR';

export const SEND_CONTACT_REQUEST = 'SEND_CONTACT_REQUEST';
export const SEND_CONTACT_SUCCESS = 'SEND_CONTACT_SUCCESS';
export const SEND_CONTACT_ERROR = 'SEND_CONTACT_ERROR';


export const fetchContactRequest = () => {
  return { type: FETCH_CONTACT_REQUEST };
};

export const fetchContactSuccess = (data) => {
  return { type: FETCH_CONTACT_SUCCESS, data};
};

export const fetchContactError = () => {
  return { type: FETCH_CONTACT_ERROR };
};

export const fetchContact = () => {
  return dispatch => {
    dispatch(fetchContactRequest());
    axios.get('/.json').then(response => {
      dispatch(fetchContactSuccess(response.data));
    }, error => {
      dispatch(fetchContactError());
    });
  }
};

export const sendContactRequest = () => {
  return { type: SEND_CONTACT_REQUEST };
};

export const sendContactSuccess = () => {
  return { type: SEND_CONTACT_SUCCESS};
};

export const sendContactError = () => {
  return { type: SEND_CONTACT_ERROR };
};

export const sendContact = value => {
  return dispatch => {
    dispatch(sendContactRequest());
    return axios.post('/.json', value).then(res => {
      dispatch(sendContactSuccess());
    }, error => {
      dispatch(sendContactError());
    })
  }
};

export const putContact = (value, id) => {
  return dispatch => {
    dispatch(sendContactRequest());
    return axios.put(`/${id}.json`, value).then(res => {
      dispatch(sendContactSuccess());
    }, error => {
      dispatch(sendContactError());
    })
  }
};

export const deleteContact = (value, id) => {
  return dispatch => {
    dispatch(sendContactRequest());
    return axios.delete(`/${id}.json`).then(res => {
      dispatch(sendContactSuccess());
    }, error => {
      dispatch(sendContactError());
    })
  }
};