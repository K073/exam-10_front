import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";
import Header from "../../components/Header/Header";
import Contacts from "../Contacts/Contacts";
import CreateContact from "../CreateContact/CreateContact";

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Switch>
          <Route path={'/'} exact component={Contacts} />
          <Route path={'/create'} exact component={CreateContact}/>
          <Route path={'/edit/:id'} exact component={CreateContact}/>
        </Switch>
      </div>
    );
  }
}

export default App;
